#pragma once

#ifndef _SHADER_H_
#define _SHADER_H_

GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path);

#endif

