// open-gl-demo.cpp : Defines the entry point for the console application.
// References used in creating this code:
// http://www.glfw.org/docs/latest/intro.html
// http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GLM/glm.hpp>
#include <GLM/ext.hpp>
#include <stdlib.h>
#include <stdio.h>
#include "common/shader.h"
using namespace glm;

void error_callback(int error, const char* description)
{
	fputs(description, stderr);
}

//Define a callback function to handle keyboard input
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_UP && action == GLFW_PRESS)
	{
		//Do something here to move a paddle up
	}

	if (key == GLFW_KEY_DOWN && action == GLFW_PRESS)
	{
		//Do something here to move a paddle down
	}
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

}




int main(void)
{
	int major, minor, revision;
	
	//output the compile time constants for version of GLFW app was compiled against
	printf("Compiled against GLFW %i.%i.%i\n",
		GLFW_VERSION_MAJOR,
		GLFW_VERSION_MINOR,
		GLFW_VERSION_REVISION);
	
	//output the runtime version of the found library
	glfwGetVersion(&major, &minor, &revision);
	printf("Running against GLFW %i.%i.%i\n", major, minor, revision);
	printf("GLFW Version String %s\n", glfwGetVersionString());
	
	//exit if we are running with a lower rev than we built
	if (GLFW_VERSION_MAJOR > major || (GLFW_VERSION_MAJOR == major && GLFW_VERSION_MINOR > minor)
		|| (GLFW_VERSION_MAJOR == major && GLFW_VERSION_MINOR == minor && GLFW_VERSION_REVISION > revision))
		exit(EXIT_FAILURE); //would like to pause to let the user see the version isue

	//register our error callback function
	glfwSetErrorCallback(error_callback);

	//Initialize GLFW and check that the init didn't fail
	if (glfwInit() == GL_FALSE)
		exit(EXIT_FAILURE);
	
	
	
	//get the current mode of the primary monitor
	const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	
	//set the Window Hints for the mode and refresh rate of the current monitor
	//trying to fiddle with the window hints such that I can click on my secondary
	//monitor and keep the OpenGL window on top on my primary monitor
	glfwWindowHint(GLFW_RED_BITS, mode->redBits);
	glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
	glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
	glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
	glfwWindowHint(GLFW_FOCUSED, GL_TRUE);
	glfwWindowHint(GLFW_FLOATING, GL_FALSE);
	glfwWindowHint(GLFW_DECORATED, GL_FALSE);
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL

	//Create and store the window called "My Title" on the primary monitor
	GLFWwindow* window = glfwCreateWindow(mode->width, mode->height, "OpenGL Learning Curve", NULL, NULL);
	
	//The createwindow function will return NULL if window creation fails
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	
	//register our keyboard callback function
	glfwSetKeyCallback(window, key_callback);

	//Set the current context to the just created window
	glfwMakeContextCurrent(window);
	
	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}
	//Set the swap interval for double buffering to 1 meaning don't process intermediate frames in the render loop
	//and only swap the buffers at the refresh rate of the monitor anything higher than 1 doesn't make sense
	//as it creates input latency, 0 on a slow machine causes screen tearing, 1 on a fast machine saves cpu
	glfwSwapInterval(1);

	//register the close on [esc] callback we need to close
	//this callback function sets the glfwWindowShouldClose return to GL_FALSE
	glfwSetKeyCallback(window, key_callback);

	float ratio;
	ratio = mode->width / (float)mode->height;

	// An array of 6 vectors which represents 6 vertices
	static const GLfloat g_paddle_vertex_buffer_data[] = {
	   -10.0f,  160.0f, 0.0f,
		10.0f,  160.0f, 0.0f,
	   -10.0f, -160.0f, 0.0f,
		10.0f,  160.0f, 0.0f,
	   -10.0f, -160.0f, 0.0f,
		10.0f, -160.0f, 0.0f,
	};

	static const GLfloat g_ball_vertex_buffer_data[] = {
	   -10.0f,  10.0f, 0.0f,
		10.0f,  10.0f, 0.0f,
	   -10.0f, -10.0f, 0.0f,
		10.0f,  10.0f, 0.0f,
	   -10.0f, -10.0f, 0.0f,
		10.0f, -10.0f, 0.0f,
	};

	mat4 Paddle1Model = mat4(1.0f);
	mat4 Paddle2Model = mat4(1.0f);
	mat4 BallModel = mat4(1.0f);
	mat4 Scale = scale(vec3(1.0f, 1.0f, 1.0f));
	mat4 Rotation = rotate(0.f, vec3(0.f, 0.f, 1.f));
	mat4 Translation = translate(vec3(0.f, 0.f, 0.f));
	mat4 View = lookAt(
		vec3(0, 0, -500), // the position of your camera, in world space
		vec3(0, 0, 0),   // where you want to look at, in world space
		vec3(0, 1, 0)        // probably vec3(0,1,0), but (0,-1,0) would make you looking upside-down, which can be great too
		);
	mat4 Projection = perspective(
		radians(100.f),         // The horizontal Field of View, in degrees : the amount of "zoom". Think "camera lens". Usually between 90� (extra wide) and 30� (quite zoomed in)
		ratio, // Aspect Ratio. Depends on the size of your window. Notice that 4/3 == 800/600 == 1280/960, sounds familiar ?
		0.1f,        // Near clipping plane. Keep as big as possible, or you'll get precision issues.
		1000.0f       // Far clipping plane. Keep as little as possible.
		);
	// Get a handle for our "MVP" uniform
	mat4 mvpPaddle1 = Projection * View * translate(vec3(-200.0f, 0.f, 0.f)) * Paddle1Model;
	mat4 mvpPaddle2 = Projection * View * translate(vec3(200.0f, 0.f, 0.f)) * Paddle2Model;
	mat4 mvpBall = Projection * View * BallModel;

	GLuint Paddle1VertexArrayID, Paddle2VertexArrayID, BallVertexArrayID;
	GLuint Paddle1vertexbufferID, Paddle2vertexbufferID, BallvertexbufferID;

	//Create vertex array object array
	glGenVertexArrays(1, &Paddle1VertexArrayID);
	glGenVertexArrays(1, &Paddle2VertexArrayID); 
	glGenVertexArrays(1, &BallVertexArrayID);
	//Create vertex buffer object array
	glGenBuffers(1, &Paddle1vertexbufferID);
	glGenBuffers(1, &Paddle2vertexbufferID);
	glGenBuffers(1, &BallvertexbufferID);
	
	//Generate new vertexbuffers and put them in vertexbuffer
	glBindVertexArray(Paddle1VertexArrayID);
	//Bind the buffer
	glBindBuffer(GL_ARRAY_BUFFER, Paddle1vertexbufferID);
	//Load our data to the vertex buffers
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_paddle_vertex_buffer_data), g_paddle_vertex_buffer_data, GL_STATIC_DRAW);
	//Compile and load my shaders
	GLuint Paddle1ProgID = LoadShaders("Paddle.vertexshader", "Paddle.fragmentshader");
	GLint Paddle1MatrixID = glGetUniformLocation(Paddle1ProgID, "MVP");

	//Generate new vertexbuffers and put them in vertexbuffer
	glBindVertexArray(Paddle2VertexArrayID);
	//Bind the buffer
	glBindBuffer(GL_ARRAY_BUFFER, Paddle2vertexbufferID);
	//Load our data to the vertex buffers
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_paddle_vertex_buffer_data), g_paddle_vertex_buffer_data, GL_STATIC_DRAW);
	GLuint Paddle2ProgID = LoadShaders("Paddle.vertexshader", "Paddle.fragmentshader");
	GLint Paddle2MatrixID = glGetUniformLocation(Paddle2ProgID, "MVP");

	//Generate new vertexbuffers and put them in vertexbuffer
	glBindVertexArray(BallVertexArrayID);
	//Bind the buffer
	glBindBuffer(GL_ARRAY_BUFFER, BallvertexbufferID);
	//Load our data to the vertex buffers
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_ball_vertex_buffer_data), g_ball_vertex_buffer_data, GL_STATIC_DRAW);
	GLuint BallProgID = LoadShaders("Ball.vertexshader", "Ball.fragmentshader");
	GLint BallMatrixID = glGetUniformLocation(BallProgID, "MVP");

	
	
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	
	while (glfwWindowShouldClose(window) != GL_TRUE)
	{
		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		// Send our transformation to the currently bound shader, in the "MVP" uniform
		// This is done in the main loop since each model will have a different MVP matrix (At least for the M part)
		glUseProgram(Paddle1ProgID);
		glUniformMatrix4fv(Paddle1MatrixID, 1, GL_FALSE, &mvpPaddle1[0][0]);
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, Paddle1vertexbufferID);
		glVertexAttribPointer(
			0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
			);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		// Use our shader

		glDisableVertexAttribArray(0);

		glUseProgram(Paddle2ProgID);
		glUniformMatrix4fv(Paddle2MatrixID, 1, GL_FALSE, &mvpPaddle2[0][0]);
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, Paddle2vertexbufferID);
		glVertexAttribPointer(
			0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
			);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		
		glDisableVertexAttribArray(0);
		
		glUseProgram(BallProgID);
		glUniformMatrix4fv(BallMatrixID, 1, GL_FALSE, &mvpBall[0][0]);
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, BallvertexbufferID);
		glVertexAttribPointer(
			0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
			);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		
		glDisableVertexAttribArray(0);
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	// Cleanup VBO and shader
	glDeleteBuffers(1, &Paddle1vertexbufferID);
	glDeleteBuffers(1, &Paddle2vertexbufferID); 
	glDeleteBuffers(1, &BallvertexbufferID); 
	glDeleteProgram(Paddle1ProgID);
	glDeleteProgram(Paddle2ProgID);
	glDeleteProgram(BallProgID);
	glDeleteVertexArrays(1, &Paddle1VertexArrayID);
	glDeleteVertexArrays(1, &Paddle2VertexArrayID);
	glDeleteVertexArrays(1, &BallVertexArrayID);

	//Destroy the window, terminate and exit with success
	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}

